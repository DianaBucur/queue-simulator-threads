import java.awt.Component;
import java.awt.event.ActionListener;

import javax.swing.*;

public class View extends JFrame{
	public JFrame frame = new JFrame("Queue Simulation");
	JLabel label1 = new JLabel("Enter no. of queues: ");
	JLabel label2 = new JLabel("Enter no. of clients: ");
	JLabel label3 = new JLabel("Enter min. time between clients: ");
	JLabel label4 = new JLabel("Enter max. time between clients: ");
	JLabel label5 = new JLabel("Enter min. service time: ");
	JLabel label6 = new JLabel("Enter max. service time: ");
	JLabel label7 = new JLabel("Enter end time: ");
	JTextField text1 = new JTextField(5);
	JTextField text2 = new JTextField(5);
	JTextField text3 = new JTextField(5);
	JTextField text4 = new JTextField(5);
	JTextField text5 = new JTextField(5);
	JTextField text6 = new JTextField(5);
	JTextField text7 = new JTextField(5);
	JButton start = new JButton("START");
	JButton stats = new JButton("STATS");
	JTextArea animation = new JTextArea(20,30);
	JTextArea log = new JTextArea(20,40);
	JPanel panel1 = new JPanel();
	JPanel panel2 = new JPanel();
	JPanel panel3 = new JPanel();
	JPanel panel4 = new JPanel();
	JPanel panel5 = new JPanel();
	JPanel panel6 = new JPanel();
	JPanel panel7 = new JPanel();
	JPanel panel8 = new JPanel();
	JPanel panel9 = new JPanel();
	JPanel panel10 = new JPanel();
	JPanel panel11 = new JPanel();
	JPanel panel12 = new JPanel();
	JPanel panel13 = new JPanel();
	JPanel panel14 = new JPanel();
	JLabel label8 = new JLabel("Average waiting time: ");
	JLabel label9 = new JLabel("Average service time: ");
	JLabel label11 = new JLabel("Empty time for queues: ");
	JLabel label12 = new JLabel("Peak hour: ");
	JTextArea label10 = new JTextArea(5,10);
	
	public View(){
		initialize();
	}
	
	public void initialize(){
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(800, 800);
		log.setEditable(false);
		JScrollPane scroll = new JScrollPane(log, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        log.setLineWrap(true);
        log.setWrapStyleWord(true);

		
		panel1.setAlignmentX(Component.CENTER_ALIGNMENT);
		label11.setAlignmentX(label11.CENTER_ALIGNMENT);
		label12.setAlignmentX(label12.CENTER_ALIGNMENT);
		text1.setAlignmentX(Component.LEFT_ALIGNMENT);
		label2.setAlignmentX(Component.LEFT_ALIGNMENT);
		text2.setAlignmentX(Component.LEFT_ALIGNMENT);
		label3.setAlignmentX(Component.LEFT_ALIGNMENT);
		text3.setAlignmentX(Component.LEFT_ALIGNMENT);
		label4.setAlignmentX(Component.LEFT_ALIGNMENT);
		text4.setAlignmentX(Component.LEFT_ALIGNMENT);
		label5.setAlignmentX(Component.LEFT_ALIGNMENT);
		text5.setAlignmentX(Component.LEFT_ALIGNMENT);
		label6.setAlignmentX(Component.LEFT_ALIGNMENT);
		text6.setAlignmentX(Component.LEFT_ALIGNMENT);
		label7.setAlignmentX(Component.LEFT_ALIGNMENT);
		text7.setAlignmentX(Component.LEFT_ALIGNMENT);
		animation.setAlignmentX(Component.RIGHT_ALIGNMENT);
		log.setAlignmentX(Component.RIGHT_ALIGNMENT);
		
		panel1.add(label1);
		panel1.add(text1);
		panel2.add(label2);
		panel2.add(text2);
		panel3.add(label3);
		panel3.add(text3);
		panel4.add(label4);
		panel4.add(text4);
		panel5.add(label5);
		panel5.add(text5);
		panel6.add(label6);
		panel6.add(text6);
		panel7.add(label7);
		panel7.add(text7);
		panel10.add(start);
		panel11.add(stats);
		panel12.add(label8);
		panel13.add(label9);
		panel14.add(label10);
		
		
		panel8.add(animation);
		panel9.add(scroll);
		
		JPanel p = new JPanel();
		p.add(panel1);
		p.add(panel2);
		p.add(panel3);
		p.add(panel4);
		p.add(panel5);
		p.add(panel6);
		p.add(panel7);
		p.add(panel10);
		p.add(panel11);
		p.add(panel12);
		p.add(panel13);
		p.add(label11);
		p.add(panel14);
		p.add(label12);
		
		JPanel p2 = new JPanel();
		p2.add(panel8);
		p2.add(panel9);
		
		JPanel p3 = new JPanel();
		p3.add(p);
		p3.add(p2);
		
		p.setLayout(new BoxLayout(p, BoxLayout.Y_AXIS));
		p2.setLayout(new BoxLayout(p2, BoxLayout.Y_AXIS));
		p3.setLayout(new BoxLayout(p3, BoxLayout.X_AXIS));
		frame.setContentPane(p3);
		frame.setVisible(true);
	}
	
	public void addStartListener(ActionListener a){
		start.addActionListener(a);
	}
	
	public void addStatsListener(ActionListener a){
		stats.addActionListener(a);
	}

	
}
