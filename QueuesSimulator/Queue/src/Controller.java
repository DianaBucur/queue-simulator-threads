
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class Controller {
	private static View view;
	private static int clientnr;
	private static int queuenr;
	private int endtime;
	private int minArrivalt;
	private int maxArrivalt;
	private int minServicet;
	private int maxServicet;
	private static Queue q[];
	
	
	public Controller(View view){
		this.view = view;
		view.addStartListener(new actionStart());
		view.addStatsListener(new actionStats());
	}
	
	public static View getView(){
		return view;
	}
	
	class actionStart implements ActionListener{
		public void actionPerformed(ActionEvent a) {
			clientnr = Integer.parseInt(view.text2.getText());
			queuenr = Integer.parseInt(view.text1.getText());
			minArrivalt = Integer.parseInt(view.text3.getText());
			maxArrivalt = Integer.parseInt(view.text4.getText());
			minServicet = Integer.parseInt(view.text5.getText());
			maxServicet = Integer.parseInt(view.text6.getText());
			endtime = Integer.parseInt(view.text7.getText());
			Scheduler s = new Scheduler (queuenr, endtime, clientnr,minServicet, maxServicet,minArrivalt,  maxArrivalt);
			s.start();
		}	
	}
	
	class actionStats implements ActionListener{

		public void actionPerformed(ActionEvent a) {
			view.label8.setText("Average waiting time: " + Scheduler.getAverageWaiting());
			view.label9.setText("Average service time: " + Scheduler.getAverageService());
			Queue queue[] = Scheduler.getQueues(); String s = "";
			for (int i = 0; i < Scheduler.getQueuenr(); i++)
				s += "Queue " + (i+1) + ": " + queue[i].emptyQueue() + '\n';
			view.label10.setText(s);
			String s2 = "Peak hour: " + Scheduler.getRushHour() + " clients at time " + Scheduler.getRushHourt();
			view.label12.setText(s2);
		}
		
	}
	
}
