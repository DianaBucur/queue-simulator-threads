
import java.util.*;

public class Scheduler extends Thread{
	private static int queuenr;
	private static int clientnr;
	private int endtime;
	private int minServicet;
	private int maxServicet;
	private int minArrivalt;
	private int maxArrivalt;
	private static int time = 1;
	private int ID = 0;
	private static int avgService = 0;
	private static int avgWaiting = 0;
	private static int terminate;
	private static int terminate2;
	private static int rushhour = 0;
	private static int rushhourt = 0;
	//private ArrayList<Client> clients = new ArrayList<Client>();
	private static Queue[] queues;
	
	public Scheduler(int queuenr, int endtime, int clientnr, int minServicet, int maxServicet, int minArrivalt, int maxArrivalt ){
		terminate2 = endtime;
		this.minArrivalt = minArrivalt;
		this.maxArrivalt = maxArrivalt;
		this.minServicet = minServicet;
		this.maxServicet = maxServicet;
		this.queuenr = queuenr;
		this.endtime = endtime;
		this.clientnr = clientnr;
		this.queues = new Queue[queuenr];
		for(int i = 0; i < queuenr; i++){
			queues[i] = new Queue(String.valueOf(i+1));
		}
	}

	public int getOptimalQueue(){
		int index = 0; int min = 500;
		for (int nr = 0; nr < queuenr; nr++){
			int totalt = 0;
			try{
			for(Client c: queues[nr].getQueue()){
					totalt += c.getServicet();
			}
			if(totalt < min){
					min = totalt;
					index = nr;
			}
			}catch(Exception e){};
		}
		return index;
	}
	
	public synchronized static int getTime(){
		return time;
	}
	
	public void startQueues(){
		for (int i = 0; i < queuenr; i++)
			queues[i].start();
			
	}
	
	public static int getAverageService(){
		return avgService/clientnr;
	}
	
	public static int getAverageWaiting(){
		return avgWaiting/clientnr;
	}
	
	public synchronized static int getTerminate(){
		return terminate2;
	}
	
	public synchronized static void rushHour(){
		int client = 0; 
		for (int i = 0; i < queuenr; i++)
			for(Client c: queues[i].getQueue())
				client++;
		if (client > rushhour){
			rushhour = client;
			rushhourt = getTime();
		}
	}
	
	public synchronized static int getRushHourt(){
		return rushhourt;
	}
	
	public synchronized static int getRushHour(){
		return rushhour;
	}
	
	public synchronized static Queue[] getQueues(){
		return queues;
	}
	
	public synchronized static int getQueuenr(){
		return queuenr;
	}
	
	public void run(){
		startQueues();
		int i = 0;
		Random rand1 = new Random();
		Random rand2 = new Random();
		int arrivalt = rand1.nextInt(maxArrivalt - minArrivalt) + minArrivalt;
		int servicet = rand2.nextInt(maxServicet - minServicet) + minServicet;
		avgService = servicet;
		int extr = 0;
		for(time = 1; time < endtime; time++){
				try {
					if( i <= clientnr){
						if (getTime() == arrivalt){
							if(i < clientnr){
								Client c = new Client(++ID, arrivalt, servicet);
								int q = getOptimalQueue();
								avgWaiting += queues[q].getWaitingt();
								if(queues[q].getWaitingt() != 0)
									avgWaiting = queues[q].getWaitingt();
								queues[q].addClient(c);
								rushHour();
								String s = "Client " + ID + " was added at queue " + ++q + " at time " + getTime() + " with service time " + c.getServicet();
								System.out.println(s);
								//Controller.update(queues, s);
								String string= "";
								String line = ""; String queue[] = new String[40];
								for(int p = 0; p < queuenr; p++){
									line = "";
									for(Client c2: queues[p].getQueue())
										line += c2.getID() + " ";
									queue[p] = "Queue " + (p+1) + ": " + line;
									string += queue[p] + '\n';
								}
								//System.out.println(string);
								Controller.getView().animation.setText(string);
								Controller.getView().log.append(s + '\n');
								
								//System.out.println("Client " + ID + " was added at queue " + ++q + " at time " + getTime() + " with service time " + c.getServicet());
								terminate = getTime() + servicet;
								rand1 = new Random();
								rand2 = new Random();
								arrivalt = getTime() + rand1.nextInt(maxArrivalt - minArrivalt) + minArrivalt;
								servicet = rand2.nextInt(maxServicet - minServicet) + minServicet;
								avgService += servicet;
								i++;
							}
							else{
								terminate2 = terminate;
							}
						}
						sleep(500);
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
		}
	}
}
