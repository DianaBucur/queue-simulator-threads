

public class Client {
	private int id;
	private int arrivet;
	private int servicet;
	
	public Client(int id, int arrivet, int servicet){
		this.id = id;
		this.arrivet = arrivet;
		this.servicet = servicet;
	}
	
	public int getID(){
		return this.id;
	}

	public int getArrivet() {
		return this.arrivet;
	}

	public void setArrivet(int arrivet) {
		this.arrivet = arrivet;
	}

	public int getServicet() {
		return this.servicet;
	}

	public void setServicet(int servicet) {
		this.servicet = servicet;
	}
	
}
